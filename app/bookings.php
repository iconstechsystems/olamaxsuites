<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class bookings extends Model
{
    protected $fillable = [
          'user_id', 'property_id', 'price', 'paymentchannel', 'checkindatetime', 'checkoutdatetime', 'bookingstatus',
    ];
}
