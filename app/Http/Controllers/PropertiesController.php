<?php

namespace App\Http\Controllers;

use App\bookings;
use Intervention\Image\ImageManagerStatic as Image;
use App\properties;
use App\gallery;
use App\locals;
use App\states;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PropertiesController extends Controller
{
    
    public function index()
    {
        $properties = properties::latest()->paginate(12);
        $title = 'Properties';
        return view('properties.allproperties')->with(['properties' => $properties, 'title' => $title]);
    }

    
    public function create()
    {
        $title = 'Create a property';
        $gallery = gallery::latest()->get();
        $states = states::all();
        $locals = locals::all();
        return view('properties.create')->with(['title' => $title, 'gallery' => $gallery, 'states' => $states, 'locals' => $locals]);
    }

    
    public function store(Request $request)
    {
        $add_property = new properties;

        $this->validate($request, [
            'banner' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            'features' => 'required',
        ]);
        
        $image = $request->file('banner');

        $image_name = time().'.'.$image->getClientOriginalExtension();
        
        $destinationPath = 'images/property/';

        $resize_image = Image::make($image->getRealPath());

        $uploaded = false;
        //dd($request->features);
        if ($resize_image->resize(400,550,function($constaint){
            $constaint->aspectRatio();
        })->save($destinationPath.$image_name)) {
            # code...
            $add_property->title = $request->title;
            $add_property->state = $request->state;
            $add_property->type = $request->type;
            $add_property->bedrooms = $request->bedrooms;
            $add_property->bathrooms = $request->bathrooms;
            $add_property->price = $request->price;
            $add_property->description = $request->description;
            $add_property->banner = $image_name;
            $add_property->videolink = $request->videolink;
            $add_property->address = $request->address;
            $add_property->features = implode(",",$request->features); 
                       
            $add_property->map = 'none';

            $add_property->save();
            $propertyid = properties::latest()->first();
            return redirect()->route('gallery.index')->with(['success_message' => 'Property has been added successfully, Add related images', 'propertyid' => $propertyid]);
        }else{
            return redirect()->route('property.create')->with(['status' => 'There was an error with the image upload. Try again']);
        }

    }

    public function adminview($id){
        $property = properties::findOrFail($id);
        return view('properties.adminview')->with(['property' => $property]);
    }
    
    public function show($id)
    {
        $property = properties::findOrFail($id);
        $featured_properties = properties::latest()->get();
        $featured_images = gallery::where('propertyid','=',$id)->get();
        $title = 'Property| '.$property->title;
        $states = states::all();
        $gallery = gallery::latest()->get();
        $states = states::all();
        $locals = locals::all();
        $checkbooking = DB::table('bookings')->selectRaw('day(checkindatetime) == now()->day');
        return view('properties.singleproperty')->with(['gallery' => $gallery, 'states' => $states, 'locals' => $locals, 'property' => $property, 'featured_properties' => $featured_properties, 'title' => $title, 'featured_images' => $featured_images, 'checkbooking' => $checkbooking]);
    }

    
    public function edit(properties $properties)
    {
        //
    }

    
    public function update(Request $request, $id)
    {
        $update_property = properties::findOrFail($id);
        if ($request->hasFile('banner')) {
            // dd('has image');
            # code...
            $this->validate($request, [
                'banner' => 'image|mimes:jpeg,png,jpg,gif,svg|max:5048',
            ]);
            
            $image = $request->file('banner');
    
            $image_name = time().'.'.$image->getClientOriginalExtension();
            
            $destinationPath = 'images/property/';
    
            $resize_image = Image::make($image->getRealPath());
    
            $uploaded = false;
            //dd($request->features);
            if ($resize_image->resize(400,550,function($constaint){
                $constaint->aspectRatio();
            })->save($destinationPath.$image_name)) {
                # code...
                $update_property->title = $request->title;
                $update_property->state = $request->state;
                $update_property->type = $request->type;
                $update_property->bedrooms = $request->bedrooms;
                $update_property->bathrooms = $request->bathrooms;
                $update_property->price = $request->price;
                $update_property->description = $request->description;
                $update_property->banner = $image_name;
                $update_property->videolink = 'none';
                $update_property->address = $request->address;
                $update_property->features = implode(",",$request->features);            
                $update_property->map = 'none';
    
                $update_property->save();
                $propertyid = properties::latest()->first();
                return redirect()->route('property.adminview',['id' => $id])->with(['success_message' => 'Property has been updated successfully',]);
            }else{
                return redirect()->route('property.adminview',['id' => $id])->with(['status' => 'There was an error with the image upload. Try again']);
            }
        }else{
            //dd($request->banner);
            # code...
            $update_property->title = $request->title;
            $update_property->state = $request->state;
            $update_property->type = $request->type;
            $update_property->bedrooms = $request->bedrooms;
            $update_property->bathrooms = $request->bathrooms;
            $update_property->price = $request->price;
            $update_property->description = $request->description;
            $update_property->videolink = 'none';
            $update_property->banner = $request->banner;
            $update_property->address = $request->address;
            $update_property->features = implode(",",$request->features);  
            //dd($update_property->features);          
            $update_property->map = 'none';

            $update_property->save();
            return redirect()->route('property.adminview',['id' => $id])->with(['success_message' => 'Property has been updated successfully',]);
        }
        
    }

    
    public function destroy(properties $properties)
    {
        //
    }
}
