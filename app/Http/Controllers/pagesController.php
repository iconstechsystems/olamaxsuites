<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\properties;
use App\states;
use DateInterval;
use DatePeriod;
use DateTime;

class pagesController extends Controller
{
    public function index(){
        $begin = new DateTime('2010-05-01');
        $end = new DateTime('2010-05-10');

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

       
        $properties = properties::all();
        $title = 'Welcome to Olamaxsuites - The Home of Haven and Tranquility.';
        $states = states::all();
        return view('index')->with(['properties' => $properties, 'title' => $title, 'states' => $states]);
        
    }

    public function agent(){
        $properties = properties::all();
        $title = 'Agent Page - Odukoya Paul';
        return view('agent')->with(['properties' => $properties, 'title' => $title]);
    }

    public function authenticate(){
        $title = 'Authentication Page';
        
        return view('auth')->with(['title' => $title]);    
    }

    public function contact(){
        $title = 'Contact Us';
        return view('contact')->with(['title' => $title]);
    }

    public function search(Request $request){
        $title = 'Search for an Apartment';
        $states = states::all();
        $all_properties = properties::all();
        $properties = properties::where('state','=',$request->state)->where('bedrooms','=',$request->bedrooms)->where('type','=',$request->type)->paginate(10);
        return view('search_results')->with(['title' => $title, 'properties' => $properties, 'states' => $states, 'all_properties' => $all_properties]);
    }
}
