<?php

namespace App\Http\Controllers;

use Intervention\Image\ImageManagerStatic as Image;
use App\gallery;
use App\properties;
use Exception;
use Illuminate\Http\Request;


class GalleryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallery = gallery::latest()->paginate(12);
        $properties = properties::latest()->get();
        return view('gallery')->with(['title' => 'My Gallery', 'gallery' => $gallery, 'properties' => $properties]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        
        $image = $request->file('image');

        $image_name = time().'.'.$image->getClientOriginalExtension();
        
        $destinationPath = public_path('/images/property');

        $resize_image = Image::make($image->getRealPath());

        $uploaded = false;

        if ($resize_image->resize(400,400,function($constaint){
            $constaint->aspectRatio();
        })->save($destinationPath.'/'.$image_name)) {

            $uploaded = true;
            $add_image = new gallery;
            $add_image->propertyid = $request->propertyid;
            $add_image->imagetitle = $image_name;
            $add_image->save();

            return redirect()->route('gallery.index')->with(['status' => 'image was uploaded successfully']);
        }else{
            return redirect()->route('gallery.index')->with(['status' => 'There was an error']);
        };


        
    }

    

    /**
     * Display the specified resource.
     *
     * @param  \App\gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function show(gallery $gallery)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function edit(gallery $gallery)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, gallery $gallery)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\gallery  $gallery
     * @return \Illuminate\Http\Response
     */
    public function destroy(gallery $gallery)
    {
        //
    }
}
