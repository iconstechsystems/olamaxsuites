<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // make sure the user has not previously registered
        $checkuser = User::where('email','=',$request->email)->get();

        if (count($checkuser)) {
            # if user email exists
            return redirect()->route('authenticate')->with(['status' => 'A User with this email already exists.']);
        }

        // check if the passwords match
        if ($request->password != $request->password_confirmation){
            return redirect()-> route('authenticate')->with(['status' => 'The Passwords entered does not match. Please make sure the passwords match and retry']);
        }

        $addUser = new User;

        $addUser->firstname = $request->firstname;
        $addUser->lastname = $request->lastname;
        $addUser->phone = $request->phone;
        $addUser->gender = $request->gender;
        $addUser->email = $request->email;
        $addUser->nationality = $request->nationality;
        $addUser->address = $request->address;
        $addUser->password =  Hash::make($request->password);
        $addUser->save();

        return redirect()->route('authenticate')->with(['status' => 'Welcome to OlamaxSuites, Proceed to login', 'email' => $request->email, 'password' => $request->password]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
