@extends('layouts.main')

@section('content')
<!--Page Banner Section start-->
<div class="page-banner-section section">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-banner-title">Add Properties</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li class="active">Add Properties</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--Page Banner Section end-->

<!--Add Properties section start-->
<div
    class="add-properties-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
    <div class="container">
        <div class="row">
            <div class="add-property-wrap col">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ session('success_message') }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session('status'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ session('status') }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                <ul class="add-property-tab-list nav mb-50">
                    <li class="working"><a href="#basic_info" data-toggle="tab">1. Basic Information</a></li>
                    <li><a href="#gallery_video" data-toggle="tab">2. Gallery & Video</a></li>
                    <li><a href="#detailed_info" data-toggle="tab">3. Detailed Information</a></li>
                </ul>

                <div class="add-property-form tab-content">
                    <form method="POST" action="{{ route('property.store') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="tab-pane show active" id="basic_info">
                            <div class="tab-body">

                                <div class="row">
                                    <div class="col-12 mb-30">
                                        <label for="property_title">Property Title</label>
                                        <input type="text" id="property_title" name="title" required value="{{ old('title') }}">
                                    </div>

                                    <div class="col-md-4 col-12 mb-30">
                                        <label for="property_address">Address</label>
                                        <input type="text" id="property_address" name="address" required value="{{ old('address') }}">
                                    </div>

                                    <div class="col-md-4 col-12 mb-30">
                                        <label for="property_address">City</label>
                                        <input type="text" id="property_address" name="city" required value="{{ old('city') }}">
                                    </div>

                                    <div class="col-md-4 col-12 mb-30">
                                        <label>Type</label>
                                        <select class="nice-select" name="type">
                                            <option>Apartment</option>
                                            <option>Cafe</option>
                                            <option>House</option>
                                            <option>Restaurant</option>
                                            <option>Store</option>
                                            <option>Villa</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-12 mb-30">
                                        <label for="property_price">Price <span>(NGN)</span></label>
                                        <input type="text" id="property_price" name="price" required value="{{ old('price') }}">
                                    </div>

                                    <div class="nav d-flex justify-content-end col-12 mb-30 pl-15 pr-15">
                                        <a href="#gallery_video" data-toggle="tab" class="btn">Next</a>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="tab-pane" id="gallery_video">
                            <div class="tab-body">


                                <div class="row">
                                    <div class="col-12 mb-30">
                                        <label>Select Banner</label>
                                        <input class="form-control" name="banner" type="file" required accept="image/*">
                                        <!--div class="dropzone"></div-->
                                    </div>

                                    <div class="nav d-flex justify-content-end col-12 mb-30 pl-15 pr-15">
                                        <a href="#detailed_info" data-toggle="tab" class="btn">Next</a>
                                    </div>
                                </div>


                            </div>
                        </div>

                        <div class="tab-pane" id="detailed_info">
                            <div class="tab-body">


                                <div class="row">
                                    <div class="col-12 mb-30">
                                        <label for="property_description">Description</label>
                                        <textarea id="property_description" name="description" required value="{{ old('description') }}"></textarea>
                                    </div>

                                    <div class="col-md-4 col-12 mb-30">
                                        <label>Bedrooms</label>
                                        <select class="nice-select" name="bedrooms" required>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                        </select>
                                    </div>

                                    <div class="col-md-4 col-12 mb-30">
                                        <label>Bathrooms</label>
                                        <select class="nice-select" name="bathrooms" required>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                            <option>5</option>
                                            <option>6</option>
                                            <option>7</option>
                                            <option>8</option>
                                            <option>9</option>
                                        </select>
                                    </div>

                                    <div class="col-12 mb-30">
                                        <h4>Other Feature</h4>
                                        <ul class="other-features">
                                            <li><input type="checkbox" name="features[]" value="1" id="air_condition"><label for="air_condition">Air
                                                    Conditioning</label></li>
                                            <li><input type="checkbox" name="features[]" value="2" id="bedding"><label for="bedding">Bedding</label>
                                            </li>
                                            <li><input type="checkbox" name="features[]" value="3" id="balcony"><label for="balcony">Balcony</label>
                                            </li>
                                            <li><input type="checkbox" name="features[]" value="4" id="cable_tv"><label for="cable_tv">Cable
                                                    TV</label></li>
                                            <li><input type="checkbox" name="features[]" value="5" id="internet"><label
                                                    for="internet">Internet</label></li>
                                            <li><input type="checkbox" name="features[]" value="6" id="parking"><label for="parking">Parking</label>
                                            </li>
                                            <li><input type="checkbox" name="features[]" value="7" id="lift"><label for="lift">Lift</label></li>
                                            <li><input type="checkbox" name="features[]" value="8" id="pool"><label for="pool">Pool</label></li>
                                            <li><input type="checkbox" name="features[]" value="9" id="dishwasher"><label
                                                    for="dishwasher">Dishwasher</label></li>
                                            <li><input type="checkbox" name="features[]" value="10" id="toaster"><label for="toaster">Toaster</label>
                                            </li>
                                            <li><input type="checkbox" name="features[]" value="11" id="gym"><label for="gym">Gym</label></li>
                                        </ul>
                                    </div>

                                    <div class="col-12">
                                        <h4>Map Information</h4>
                                        <div class="row mt-20">
                                            <div class="col-lg-6 col-12 mb-30">
                                                <label for="map_address">Google Maps Address</label>
                                                <input type="text" id="map_address" name="map" >
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="nav d-flex justify-content-end col-12 mb-30 pl-15 pr-15">
                                        <button class="property-submit btn" type="submit">Add Property</button>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<!--Add Properties section end-->
@endsection
