@extends('layouts.main')

@section('content')
    <!--Page Banner Section start-->
    <div class="page-banner-section section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="page-banner-title">Properties</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active">Properties</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Page Banner Section end-->

    <!--New property section start-->
    <div class="property-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
        <div class="container">
            
            <div class="row">
               
                <!--Property start-->
                @foreach ($properties as $item)
                <div class="property-item col-lg-4 col-md-6 col-12 mb-40">
                    <div class="property-inner">
                        <div class="image">
                            <a href="{{ route('property.view', ['id' => $item->id]) }}"><img src="{{ asset('storage/property_banners/'.$item->banner.'') }}" alt="olamax suites {{ $item->title }}"  style="height:300px; width:100%;"></a>
                            <ul class="property-feature">
                                <li>
                                    <span class="bed"><img src="{{ asset('images/bed.png') }}" alt="">{{ $item->bedrooms }}</span>
                                </li>
                                <li>
                                    <span class="bath"><img src="{{ asset('images/bath.png') }}" alt="">{{ $item->bathrooms }}</span>
                                </li>   
                            </ul>
                        </div>
                        <div class="content">
                            <div class="left">
                                <h3 class="title"><a href="{{ route('property.view', ['id' => $item->id]) }}">{{ $item->title }}</a></h3>
                                <span class="price"><b>NGN <?php echo floatval($item->price); ?><span>/D</span></b></span>
                                <span class="location"><img src="{{ asset('images/marker.png') }}" alt="">{{ $item->address }}, {{ $item->city }}</span>
                            </div>
                            <div class="right">
                                <div class="type-wrap">
                                    
                                    <span class="type">For Rent</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--Property end-->
                
            </div>
            
            <div class="row mt-20">
                <div class="col">
                    <ul class="page-pagination">
                        <li><a href="#"><i class="fa fa-angle-left"></i> Prev</a></li>
                        <li class="active"><a href="#">01</a></li>
                        <li><a href="#">02</a></li>
                        <li><a href="#">03</a></li>
                        <li><a href="#">04</a></li>
                        <li><a href="#">05</a></li>
                        <li><a href="#"><i class="fa fa-angle-right"></i> Next</a></li>
                    </ul>
                </div>
            </div>
            
        </div>
    </div>
    <!--New property section end-->
@endsection