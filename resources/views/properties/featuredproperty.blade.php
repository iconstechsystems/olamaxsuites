@extends('layouts.main')

@section('content')
<!--Page Banner Section start-->
<div class="page-banner-section section">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-banner-title">Add Properties</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li class="active">Add Properties</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--Page Banner Section end-->

<!--Add Properties section start-->
<div class="add-properties-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
    <div class="container">
        <div class="row">
            @if (session('success_message'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong>{{ session('success_message') }}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            <form action="{{ route('property.addpostfeatured') }}" method="post">
                @csrf
                <div class="col-12 mb-30">
                    <div class="form-group">
                        <label>Property</label>
                        <select>
                            @foreach ($properties as $item)
                                <option value="{{ $item->id }}" @if (session('id'))
                                    @if ($item->id == session('id'))
                                        selected
                                    @endif
                                @endif>{{ $item->title }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-control">
                        <label>Select Related Images</label>
                        <input class="form-control" accept="image/*" name="featured" required>
                    </div>
                    <br>
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
<!--Add Properties section end-->

@endsection