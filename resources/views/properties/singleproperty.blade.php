@extends('layouts.main')

@section('content')
<!--Page Banner Section start-->
<div class="page-banner-section section"
    style="background-image: url({{ asset('storage/property_banners/'.$property->banner.'') }})">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-banner-title">Properties</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li><a href="{{ route('property.index') }}">Properties</a></li>
                    <li class="active">{{ $property->title }}</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--Page Banner Section end-->

<!--New property section start-->
<div
    class="property-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
    <div class="container">
        <div class="row">

            <div class="col-lg-8 col-12 order-1 order-lg-2 mb-sm-50 mb-xs-50">
                <div class="row">

                    <!--Property start-->
                    <div class="single-property col-12 mb-50">
                        <div class="property-inner">

                            <div class="head">
                                <div class="left">
                                    <h1 class="title">{{ $property->title }}</h1>
                                    <span class="location"><img src="{{ asset('images/marker.png') }}"
                                            alt="">{{ $property->address }}, {{ $property->city }}</span>
                                </div>
                                <div class="right">
                                    <div class="type-wrap">
                                        <span class="price">N
                                            {{ number_format($property->price,2) }}<span>Day</span></span>
                                        <span class="type">For Rent</span>
                                    </div>
                                </div>
                            </div>

                            <div class="image mb-30">
                                <div class="single-property-gallery">
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                </div>
                                <div class="single-property-thumb">
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                    <div class="item"><img
                                            src="{{ asset('storage/property_banners/'.$property->banner.'') }}" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="content">

                                <h3>Description</h3>

                                <p>{{ $property->description }}</p>


                                <div class="row mt-30 mb-30">

                                    <div class="col-md-5 col-12 mb-xs-30">
                                        <h3>Condition</h3>
                                        <ul class="feature-list">
                                            <li>
                                                <div class="image"><img src="{{ asset('images/area.png') }}" alt="">
                                                </div>Area 550 sqft
                                            </li>
                                            <li>
                                                <div class="image"><img src="{{ asset('images/bed.png') }}" alt="">
                                                </div>{{ $property->bedrooms }} Bedrooms
                                            </li>
                                            <li>
                                                <div class="image"><img src="{{ asset('images/bath.png') }}" alt="">
                                                </div>{{ $property->bathrooms }} Bathroom
                                            </li>
                                            <li>
                                                <div class="image"><img src="{{ asset('images/kitchen.png') }}" alt="">
                                                </div>1 Kitchen
                                            </li>
                                        </ul>
                                    </div>

                                    <div class="col-md-7 col-12">
                                        <h3>Amenities</h3>
                                        <ul class="amenities-list">
                                            <li>Air Conditioning</li>
                                            <li>Bedding</li>
                                            <li>Balcony</li>
                                            <li>Cable TV</li>
                                            <li>Internet</li>
                                            <li>Parking</li>
                                            <li>Lift</li>
                                            <li>Pool</li>
                                            <li>Dishwasher</li>
                                            <li>Toaster</li>
                                        </ul>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-12 mb-30">
                                        <h3>Video</h3>
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <iframe class="embed-responsive-item"
                                                src="https://www.youtube.com/embed/8CbvItGX7Vk"></iframe>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <h3>Location</h3>
                                        <div class="embed-responsive embed-responsive-16by9">
                                            <div id="single-property-map" class="embed-responsive-item google-map"
                                                data-lat="40.740178" data-Long="-74.190194"></div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!--Property end-->

                    <!-- disqus -->
                    <div id="disqus_thread"></div>
                    <script>
                        

                        var disqus_config = function () {
                        this.page.url = '{{ Request::url() }}';  // Replace PAGE_URL with your page's canonical URL variable
                        this.page.identifier = '{{ $property->id }}'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                        };
                        
                        (function () { // DON'T EDIT BELOW THIS LINE
                            var d = document,
                                s = d.createElement('script');
                            s.src = 'https://olamaxsuites.disqus.com/embed.js';
                            s.setAttribute('data-timestamp', +new Date());
                            (d.head || d.body).appendChild(s);
                        })();

                    </script>
                    <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments
                            powered by Disqus.</a></noscript>

                </div>
            </div>

            <div class="col-lg-4 col-12 order-2 order-lg-1 pr-30 pr-sm-15 pr-xs-15">

                <!--Sidebar start-->
                <div class="sidebar">
                    <h4 class="sidebar-title"><span class="text">Search Property</span><span class="shape"></span></h4>


                    <!--Property Search start-->
                    <div class="property-search sidebar-property-search">

                        <form action="#">
                            <div>
                                <select class="nice-select">
                                    <option>All Cities</option>
                                    <option>Athina</option>
                                    <option>Austin</option>
                                    <option>Baytown</option>
                                    <option>Brampton</option>
                                    <option>Cedar Hill</option>
                                    <option>Chester</option>
                                    <option>Chicago</option>
                                    <option>Coleman</option>
                                    <option>Corpus Christi</option>
                                    <option>Dallas</option>
                                    <option>distrito federal</option>
                                    <option>Fayetteville</option>
                                    <option>Galveston</option>
                                    <option>Jersey City</option>
                                    <option>Los Angeles</option>
                                    <option>Midland</option>
                                    <option>New York</option>
                                    <option>Odessa</option>
                                    <option>Reno</option>
                                    <option>San Angelo</option>
                                    <option>San Antonio</option>
                                </select>
                            </div>

                            <div>
                                <select class="nice-select">
                                    <option>For Rent</option>
                                    <option>For Sale</option>
                                </select>
                            </div>

                            <div>
                                <select class="nice-select">
                                    <option>Type</option>
                                    <option>Apartment</option>
                                    <option>Cafe</option>
                                    <option>House</option>
                                    <option>Restaurant</option>
                                    <option>Store</option>
                                    <option>Villa</option>
                                </select>
                            </div>

                            <div>
                                <select class="nice-select">
                                    <option>Bedrooms</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                </select>
                            </div>

                            <div>
                                <select class="nice-select">
                                    <option>Bathrooms</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                </select>
                            </div>

                            <div>
                                <button>search</button>
                            </div>

                        </form>

                    </div>
                    <!--Property Search end-->

                </div>
                <!--Sidebar end-->

                <!--Sidebar start-->
                <div class="sidebar">
                    <h4 class="sidebar-title"><span class="text">Featured Property</span><span class="shape"></span>
                    </h4>

                    <!--Sidebar Property start-->
                    <div class="sidebar-property-list">

                        @foreach ($featured_properties as $item)
                        @if ($item->id != $property->id)
                        <div class="sidebar-property">
                            <div class="image">
                                <span class="type">For Rent</span>
                                <a href="#"><img src="{{ asset('storage/property_banners/'.$item->banner.'') }}"
                                        alt=""></a>
                            </div>
                            <div class="content">
                                <h5 class="title"><a href="#">{{ $item->title }}</a></h5>
                                <span class="location"><img src="{{ asset('images/marker.png') }}"
                                        alt="">{{ $item->address }}, {{ $item->city }}</span>
                                <span class="price">N {{ number_format($item->price,2) }} <span>Day</span></span>
                            </div>
                        </div>
                        @endif
                        @endforeach

                    </div>
                    <!--Sidebar Property end-->

                </div>

                <!--Sidebar start-->
                <div class="sidebar">
                    <h4 class="sidebar-title"><span class="text">Agent</span><span class="shape"></span></h4>

                    <!--Sidebar Agents start-->
                    <div class="sidebar-agent-list">

                        <div class="sidebar-agent">
                            <div class="image">
                                <a href="#"><img src="{{ asset('images/agent/agent-paul.jpeg') }}" alt=""></a>
                            </div>
                            <div class="content">
                                <h5 class="title"><a href="{{ route('agent') }}">Odukoya Paul</a></h5>
                                <a href="tel:+2349033826711" class="phone">+234 903 382 6711</a>
                                <span class="properties">{{ count($featured_properties) }} Properties</span>
                                <div class="social">
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                    <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>

                    </div>
                    <!--Sidebar Agents end-->

                </div>

                <!--Sidebar start-->


            </div>

        </div>
    </div>
</div>
<!--New property section end-->
@endsection
