@extends('layouts.main')

@section('content')
<!--Page Banner Section start-->
<div class="page-banner-section section">
    <div class="container">
        <div class="row">
            <div class="col">
                <h1 class="page-banner-title">My Account</h1>
                <ul class="page-breadcrumb">
                    <li><a href="{{ route('index') }}">Home</a></li>
                    <li class="active">My Account</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<!--Page Banner Section end-->
<div class="login-register-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20">
    <div class="container">
        <div class="row row-25">
            <div class="col-lg-12 col-md-12">
                @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @elseif(session('status'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ session('status') }}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
            <br>
            <div class="col-lg-12 col-md-12">

                <form action="{{ route('gallery.store') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <label>Select Property</label>
                            <select name="propertyid" class="form-control">
                                @foreach ($properties as $item)
                                    <option value="{{ $item->id }}" 
                                        @if (session('id'))
                                            @if ($item->id == session('id'))
                                                selected
                                            @endif
                                        @endif
                                        >{{ $item->title }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <label for="image">Add a featured image</label>
                            <input type="file" name="image" id="image" class="form-control" accept="image/*">
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary"> Submit </button>
                </form>
                
            </div>
        </div>
        <br>
        <div class="row row-25">
            @foreach ($gallery as $item)
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="card">
                        <img class="card-img-top" src="{{ asset('images/property/'.$item->imagetitle.'') }}" alt="Card image cap">
                        <div class="card-body">
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary">View</button>
                                <button type="button" class="btn btn-danger">Delete</button>
                              </div>
                        </div>
                    </div>
                </div>
            @endforeach
            <br>
            {{ $gallery->links() }}
        </div>
    </div>
</div>

@endsection