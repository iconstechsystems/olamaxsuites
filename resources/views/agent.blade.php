@extends('layouts.main')

@section('content')
    <!--Page Banner Section start-->
    <div class="page-banner-section section">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="page-banner-title">Agent Details</h1>
                    <ul class="page-breadcrumb">
                        <li><a href="{{ route('index') }}">Home</a></li>
                        <li class="active">Agent Details</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Page Banner Section end-->

    <!--Agent Section start-->
    <div class="agent-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20">
        <div class="container">
            
            <div class="row row-25">

                <!--Agent Image-->
                <div class="col-lg-5 col-12 mb-30">
                    <div class="agent-image">
                        <img src="{{ asset('images/agent/agent-paul.jpeg') }}" alt="">
                    </div>
                </div>

                <!--Agent Content-->
                <div class="col-lg-7 col-12">
                    <div class="agent-content">
                        <h3 class="title">Odukoya Paul</h3>
                        <p>Odukoya Paul is a Specialist Real Estate Agent with 8 years of Experience in Real Estate field. He achive success with his honesty,determination, hardwork and commetment. </p>
                        <div class="row">
                            <div class="col-md-6 col-12 mb-30">
                                <h4>Personal Info</h4>
                                <ul>
                                    <li><i class="pe-7s-map"></i>15B TOWER B, EKO PEARL TOWER, VICTORIA ISLAND, LAGOS</li>
                                    <li><i class="pe-7s-phone"></i><a href="tel:+2349033826711">+234 903 382 6711</a></li>
                                    <li><i class="pe-7s-mail-open"></i><a href="mailto:paul@olamaxsuites.com">paul@olamaxsuites.com</a></li>
                                    <li><i class="pe-7s-photo"></i>{{ count($properties) }} Properties</li>
                                </ul>
                                <div class="social">
                                    <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                                    <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
                                    <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                                    <a href="#" class="google"><i class="fa fa-google-plus"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>
    <!--Agent Section end-->

    <!--Feature property section start-->
    <div class="property-section section pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
        <div class="container">
            
            <!--Section Title start-->
            <div class="row">
                <div class="col-md-12 mb-60 mb-xs-30">
                    <div class="section-title center">
                        <h1>Odukoya Paul's Properties</h1>
                    </div>
                </div>
            </div>
            <!--Section Title end-->
            
            <div class="row">
               
                <!--Property Slider start-->
                <div class="property-carousel section">

                    <!--Property start-->
                    @foreach ($properties as $item)
                    <div class="property-item col">
                        <div class="property-inner">
                            <div class="image">
                                <a href="{{ route('property.view', ['id' => $item->id]) }}"><img src="{{ asset('storage/property_banners/'.$item->banner.'') }}" alt="olamax suites victoria island shortlet ikoyi lagos hotel club" style="height:300px; width:100%;"></a>
                                <ul class="property-feature">
                                    
                                    <li>
                                        <span class="bed"><img src="{{ asset('images/bed.png') }}" alt="">{{ $item->bedrooms }}</span>
                                    </li>
                                    <li>
                                        <span class="bath"><img src="{{ asset('images/bath.png') }}" alt="">{{ $item->bathrooms }}</span>
                                    </li>
                                    
                                </ul>
                            </div>
                            <div class="content">
                                <div class="left">
                                    <h3 class="title"><a href="{{ route('property.view', ['id' => $item->id]) }}">{{ $item->title }}</a></h3>
                                    <span class="price"><b>N {{ number_format($item->price, 2) }}<span> / D</span></b></span>
                                    <span class="location"><img src="{{ asset('images/marker.png') }}" alt="">{{ $item->address }}, {{ $item->city }}</span>
                                </div>
                                <div class="right">
                                    <div class="type-wrap">
                                        
                                        <span class="type">For Rent</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <!--Property end-->

                                     

                </div>
                <!--Property Slider end-->
                
            </div>
            
        </div>
    </div>
    <!--Feature property section end-->
@endsection