<!doctype html>
<html class="no-js" lang="zxx">

@include('layouts.header')

<body>

    <div id="main-wrapper">

        <!--Header section start-->
        @include('layouts.navigation')
        <!--Header section end-->

        @yield('content')

        <!--Footer section start-->
        @include('layouts.footer')
        <!--Footer section end-->
    </div>

    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- All jquery file included here -->
    @include('layouts.scripts')

</body>

</html>
