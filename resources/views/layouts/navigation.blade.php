<header class="header header-sticky">
    <div class="header-bottom menu-center">
        <div class="container">
            <div class="row justify-content-between">
                
                <!--Logo start-->
                <div class="col mt-10 mb-10">
                    <div class="logo">
                        <a href="{{ route('index') }}"><img src="{{ asset('images/logo.jpg') }}" alt="olamaxsuites homes like heaven"></a>
                    </div>
                </div>
                <!--Logo end-->
                
                <!--Menu start-->
                <div class="col d-none d-lg-flex">
                    <nav class="main-menu">
                        <ul>
                            <li><a href="{{ route('index') }}">Home</a>
                                
                            </li>
                            <li><a href="{{ route('property.index') }}">Properties</a>
                            </li>
                            <li><a href="{{ route('agent') }}">Agents</a>
                            </li>
                            <li><a href="{{ route('contact') }}">Contact Us</a>
                            </li>
                        </ul>
                    </nav>
                </div>
                <!--Menu end-->
                
                <!--User start-->
                <div class="col mr-sm-50 mr-xs-50">
                    <div class="header-user">
                        @guest
                        <a href="{{ route('authenticate') }}" class="user-toggle"><i class="pe-7s-user"></i><span>***</span></a>
                        @else
                        <a href="{{ route('home') }}" class="user-toggle"><i class="pe-7s-user"></i><span><i class="fa fa-user-circle-o"></i>+++</span></a>
                        @endguest
                    </div>
                </div>
                <!--User end-->
            </div>
            
            <!--Mobile Menu start-->
            <div class="row">
                <div class="col-12 d-flex d-lg-none">
                    <div class="mobile-menu"></div>
                </div>
            </div>
            <!--Mobile Menu end-->
            
        </div>
    </div>
</header>