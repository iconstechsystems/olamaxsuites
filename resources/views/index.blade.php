@extends('layouts.main')

@section('content')
<!--slider section start-->
<div class="hero-section section position-relative">

    <!--Hero Item start-->
    <div class="hero-item align-items-end">
        <ul class="kenburns-slideshow">
            <li><img src="{{ asset('images/sliders/1.jpg') }}" alt=""></li>
            <li><img src="{{ asset('images/sliders/2.jpg') }}" alt=""></li>
            <li><img src="{{ asset('images/sliders/3.jpg') }}" alt=""></li>
            <li><img src="{{ asset('images/sliders/4.jpg') }}" alt=""></li>
            <li><img src="{{ asset('images/sliders/5.jpg') }}" alt=""></li>
            <li><img src="{{ asset('images/sliders/6.jpg') }}" alt=""></li>
        </ul>
        <div class="container">
            <div class="row">
                <div class="col-12">

                    <!--Property Search start-->
                    <div class="property-search hero-property-search">
                        <h1 class="title">Find Your <span>Dream</span> Apartment</h1>
                        <form action="{{ route('search') }}" method="POST">
                            {{ csrf_field() }}
                            <div>
                                <select class="nice-select">
                                    <option>Select a State</option>
                                    @foreach ($states as $item)
                                        <option value="{{ $item->name }}">{{ $item->name.' state' }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <!--div>
                                <select class="nice-select">
                                    <option>For Booking</option>
                                    <option>For Sale</option>
                                </select>
                            </div-->

                            <div>
                                <select class="nice-select">
                                    <option>Type</option>
                                    <option>Apartment</option>
                                    <option>Cafe</option>
                                    <option>House</option>
                                    <option>Restaurant</option>
                                    <option>Store</option>
                                    <option>Villa</option>
                                </select>
                            </div>

                            <div>
                                <select class="nice-select">
                                    <option>Bedrooms</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                    <option>6</option>
                                </select>
                            </div>

                            <div>
                                <button type="submit">search</button>
                            </div>

                        </form>

                    </div>
                    <!--Property Search end-->

                </div>
            </div>
        </div>
    </div>
    <!--Hero Item end-->

</div>
<!--slider section end-->

<!--Welcome Khonike - Real Estate Bootstrap 4 Templatesection-->
<div
    class="feature-section feature-section-border-bottom section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20">
    <div class="container">
        <div class="row">

            <!--Feature start-->
            <div class="col-lg-3 col-sm-6 col-12 mb-30">
                <div class="feature-2">
                    <div class="icon"><i class="pe-7s-shield"></i></div>
                    <div class="content">
                        <h4>Secure</h4>
                        <p>We pride ourselves on making sure our customemrs get total safety</p>
                    </div>
                </div>
            </div>
            <!--Feature end-->

            <!--Feature start-->
            <div class="col-lg-3 col-sm-6 col-12 mb-30">
                <div class="feature-2">
                    <div class="icon"><i class="pe-7s-piggy"></i></div>
                    <div class="content">
                        <h4>Affordable</h4>
                        <p>With Olamaxsuites, you wont exceed your budget, Just your expectations</p>
                    </div>
                </div>
            </div>
            <!--Feature end-->

            <!--Feature start-->
            <div class="col-lg-3 col-sm-6 col-12 mb-30">
                <div class="feature-2">
                    <div class="icon"><i class="pe-7s-map"></i></div>
                    <div class="content">
                        <h4>Accessible</h4>
                        <p>All our apartments are suitated in choice areas that have accessible road networks</p>
                    </div>
                </div>
            </div>
            <!--Feature end-->

            <!--Feature start-->
            <div class="col-lg-3 col-sm-6 col-12 mb-30">
                <div class="feature-2">
                    <div class="icon"><i class="pe-7s-bed"></i></div>
                    <div class="content">
                        <h4>Convenient</h4>
                        <p>Convenience is the aim of Hospitality and we are sure to provide the best.</p>
                    </div>
                </div>
            </div>
            <!--Feature end-->

        </div>
    </div>
</div>
<!--Welcome Khonike - Real Estate Bootstrap 4 Templatesection end-->

<!--Feature property section start-->
<div
    class="property-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50">
    <div class="container">

        <!--Section Title start-->
        <div class="row">
            <div class="col-md-12 mb-60 mb-xs-30">
                <div class="section-title center">
                    <h1>Featured Property</h1>
                </div>
            </div>
        </div>
        <!--Section Title end-->

        <div class="row">

            <!--Property Slider start-->
            <div class="property-carousel section">

                <!--Property start-->
                @foreach ($properties as $item)
                <div class="property-item col">
                    <div class="property-inner">
                        <div class="image">
                            <a href="{{ route('property.view', ['id' => $item->id ]) }}"><img src="{{ asset('images/property/'.$item->banner.'') }}"
                                    alt="olamax suites olamaxsuites.com {{ $item->title }}" style="height: 300px; width: 100%;"></a>
                            <ul class="property-feature">
                                <li>
                                    <span class="bed"><img src="{{ asset('images/bed.png') }}" alt="">{{ $item->bedrooms }}</span>
                                </li>
                                <li>
                                    <span class="bath"><img src="{{ asset('images/bath.png') }}" alt="">{{ $item->bathrooms }}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="left">
                                <h3 class="title"><a href="{{ route('property.view', ['id' => $item->id ]) }}">{{ $item->title }}</a></h3>
                                <span class="price"><b>N <?php echo floatval($item->price); ?><span> / Day</span></b></span>
                                <span class="location"><img src="{{ asset('images/marker.png') }}" alt="">{{ $item->address }}, {{ $item->city }}</span>
                            </div>
                            <div class="right">
                                <div class="type-wrap">
                                    
                                    <span class="type">For Booking</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
                <!--Property end-->

            </div>
            <!--Property Slider end-->

        </div>
    </div>
</div>
<!--Feature property section end-->


<!--Services section start-->
<div
    class="service-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-70 pb-lg-50 pb-md-40 pb-sm-30 pb-xs-20">
    <div class="container">

        <!--Section Title start-->
        <div class="row">
            <div class="col-md-12 mb-60 mb-xs-30">
                <div class="section-title center">
                    <h1>Featured Services</h1>
                </div>
            </div>
        </div>
        <!--Section Title end-->

        <div class="row row-30 align-items-center">
            <div class="col-lg-5 col-12 mb-30">
                <div class="property-slider-2">
                    @foreach ($properties as $item)
                    <div class="property-2">
                        <div class="property-inner">
                            <a href="{{ route('property.view', ['id' => $item->id]) }}" class="image"><img
                                    src="{{ asset('images/property/'.$item->banner.'') }}" alt="{{ $item->title }} olamax suites victoria island hotel lagos hotel club eko atlantic"></a>
                            <div class="content">
                                <h4 class="title"><a href="{{ route('property.view', ['id' => $item->id]) }}">{{ $item->title }}</a>
                                </h4>
                                <span class="location">{{ $item->address.', '.$item->city }}</span>
                                <h4 class="type">Rent <span>N <?php echo floatval($item->price); ?> <span>Day</span></span></h4>
                                <ul>
                                    <li>{{ $item->bedrooms }} Bed</li>
                                    <li>{{ $item->bathrooms }} Bath</li>
                                    <li><a href="{{ route('property.view', ['id' => $item->id]) }}" style="color: white"><i class="fa fa-search"></i>&nbsp; View Apartment</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    @endforeach

                </div>
            </div>
            <div class="col-lg-7 col-12">
                <div class="row row-20">

                    <ul style="margin-left: 20px;">
                        <li><i class="fa fa-check-circle" style="color: blue;"></i>&nbsp;Well serviced apartments in convenient locations</li>
                        <li><i class="fa fa-check-circle" style="color: blue;"></i>&nbsp;Airport pick-up and drop-off</li>
                        <li><i class="fa fa-check-circle" style="color: blue;"></i>&nbsp;24/7 Concierge Services</li>
                        <li><i class="fa fa-check-circle" style="color: blue;"></i>&nbsp;Security Personnel</li>
                        <li><i class="fa fa-check-circle" style="color: blue;"></i>&nbsp;Chef Services</li>
                        <li><i class="fa fa-check-circle" style="color: blue;"></i>&nbsp;Lagos Night Life</li>
                    </ul>

                </div>
            </div>
        </div>

    </div>
</div>
<!--Services section end-->


<!--CTA Section start-->
<div class="cta-section section pt-100 pt-lg-80 pt-md-70 pt-sm-60 pt-xs-50 pb-100 pb-lg-80 pb-md-70 pb-sm-60 pb-xs-50"
    style="background-image: url({{ asset('images/bg/cta-bg.jpg') }})">
    <div class="container">
        <div class="row">
            <div class="col">

                <!--CTA start-->
                <div class="cta-content text-center">
                    <h1>Do you need something <span>Unique?</span></h1>
                    <div class="buttons">
                        <a href="mailto:olamaxsuites@gmail.com"><i class="fa fa-envelope"></i>&nbsp;Send us an Email</a>
                        <a href="tel:+2349033826711"><i class="fa fa-phone"></i>&nbsp;Call us</a>
                    </div>
                </div>
                <!--CTA end-->

            </div>
        </div>
    </div>
</div>
<!--CTA Section end-->


@endsection
