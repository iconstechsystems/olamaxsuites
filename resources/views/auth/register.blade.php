<form action="{{ route('register_user') }}" method="POST">
    @csrf
    <div class="row">
        <div class="col-12 mb-30"><input type="text" placeholder="First Name" required name="firstname"></div>
        <div class="col-12 mb-30"><input type="text" placeholder="Last Name" required name="lastname"></div>
        <div class="col-12 mb-30"><input type="email" placeholder="Email" required name="email"></div>
        <div class="col-12 mb-30"><input type="text" placeholder="Phone" required name="phone"></div>
        <div class="col-12 mb-30"><input type="text" placeholder="Nationality" required name="nationality"></div>
        <div class="col-12 mb-30"><input type="text" placeholder="Address" required name="address"></div>
        <div class="col-12 mb-30">
            <select name="gender" class="form-control" required>
                <option>Male</option>
                <option>Female</option>
            </select>
        </div>
        <div class="col-12 mb-30"><input type="password" placeholder="Password" required name="password" min="8"></div>
        <div class="col-12 mb-30"><input type="password" placeholder="Confirm Password" required min="8" name="password_confirmation"></div>
        <div class="col-12 mb-30">
            <ul>
                <li><input type="checkbox" id="register_agree" required><label for="register_agree">I agree with your <a href="#">Terms & Conditions</a></label></li>
            </ul>
        </div>
        
        <div class="col-12"><button class="btn" type="submit">Register</button></div>
    </div>
</form>