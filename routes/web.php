<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'pagesController@index')->name('index');

Route::get('/property', 'PropertiesController@index')->name('property.index');


Route::get('/authenticate', 'pagesController@authenticate')->name('authenticate');

Route::post('/register_user', 'UsersController@store')->name('register_user');

Route::get('/contact', 'pagesController@contact')->name('contact');

Route::get('/agent', 'pagesController@agent')->name('agent');

Auth::routes();

// admin routes
Route::group(['middleware' => ['auth']], function () {
    Route::get('/property/create', 'PropertiesController@create')->name('property.create');

    Route::post('/property/create', 'PropertiesController@store')->name('property.store');

    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/mygallery', 'GalleryController@index')->name('gallery.index');

    Route::post('/mygallery', 'GalleryController@store')->name('gallery.store');
});

Route::get('/property/{id}', 'PropertiesController@show')->name('property.view');




